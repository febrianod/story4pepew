from django.urls import path
from django.conf.urls import url
from . import views
from .views import add_activity

app_name = 'myapp'

urlpatterns = [path('', views.index, name='index'), url(r'add_activity/$', add_activity, name='add_activity'),]
